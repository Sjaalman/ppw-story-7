from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index

# Create your tests here.
class TestUrls(TestCase):
    def setUp(self):
        self.index_url = reverse("story7:index")

    def test_mini_profile_works(self):
        index_url_func = resolve(self.index_url)
        self.assertEquals(index_url_func.func, index)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_url = reverse("story7:index")

    def test_index_GET(self):
        response = self.client.get(self.index_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "index.html")

