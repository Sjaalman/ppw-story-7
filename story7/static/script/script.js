$(document).ready(function() {
    $("h3").click(function() {
      if ($(this).next().css("display") === "none") {
        $(this).next().css({"display" : "block"})
      } else {
        $(this).next().css({"display" : "none"})
      }
    }).find(".up, .down").click(function(e) {
        return false
    })

    $(".up").click(function() {
        var grandpa = $(this).parent().parent()
        grandpa.insertBefore(grandpa.prev())
    })

    $(".down").click(function() {
        var grandpa = $(this).parent().parent()
        grandpa.insertAfter(grandpa.next())
    })
});